# alberto-cavadia-back

"Recomendaciones: Debes enviar la respuesta de la prueba en un sólo archivo .zip, drive o en un solo repositorio (GitHub, Bitbucket, etc.). Cualquier forma que uses para enviar tu prueba, debes enviarla en un solo archivo o repositorio, el cual se debe nombrar nombre-apellido-back. Ejemplo: diego-rios-back"

La solucion a los tres ejercicios de la [prueba backend](https://drive.google.com/file/d/0By3FUmkCvFI_NUxNQ056NXc5a2QwbEgxcVRIT1haaDR6OXNZ/view) se encuentran en este respositorio:

* Preguntas teóricas se encuentran este MarkDown.
* El "Cube Summation" challenge de HackerRank se encuentra en el branch challenge/cubeSummation.
* El refactor en PHP se encuentra en el branch refactor/controllerMethod.

PREGUNTAS (10 puntos)
Responde y envía en un documento las siguientes preguntas:

1. ¿En qué consiste el principio de responsabilidad única? ¿Cuál es su propósito?
   
    Consiste en mantener una aplicación modular, donde cada módulo de desarrollo sea responsable de un solo aspecto de la funcionalidad que ofrece la aplicación en su totalidad  
   Al mantener el desarrollo modular facilitas la localización de fallas, evitas comprometer otros aspectos de la aplicación, mejoras la legibilidad. En general, incrementas la mantenibilidad de la aplicación y la hace más robusta.

2. ¿Qué características tiene según tu opinión “buen” código o código limpio?

    Es en un aspecto amplio, consistente, legible y mantiene los principios de responsabilidad única y DRY(Don´t Repeat Yourself) implicando reusabilidad, dinamismo y encapuslación de segmentos funcionales.